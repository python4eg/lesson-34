import scrapy


class SpiderSpider(scrapy.Spider):
    name = 'spider'
    allowed_domains = ['localhost:63342']
    start_urls = ['http://localhost:63342/parsing/index.html?_ijt=n6u89i3v6h8ghj50706oukj58o']
    page = 1

    def parse(self, response):
        print('URL:')
        print(response.url)
        books = response.xpath('//li[@class="col-xs-6 col-sm-4 col-md-3 col-lg-3"]/article')
        for book in books:
            name = book.xpath('h3/a/@title').extract_first()
            price = book.xpath('div[@class="product_price"]/p[@class="price_color"]/text()').extract_first()
            href = self.start_urls[0] + book.xpath('h3/a/@href').extract_first()
            yield {
                'name': name,
                'price': price,
                'url': href
            }
        next_page = response.xpath('//ul[@class="pager"]/li[@class="next"]/a/@href').extract_first()
        if next_page:
            if 'catalogue/' in next_page:
                new_url = self.start_urls[0] + '/' + next_page
            else:
                new_url = self.start_urls[0] + '/catalogue/' + next_page
            yield scrapy.Request(new_url, callback=self.parse)

