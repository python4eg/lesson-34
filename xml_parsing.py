from xml.etree import ElementTree

tree = ElementTree.parse('1_xml.html')
root = tree.getroot()
print(dir(root))
print(root.tag)
for elem in root:
    print(elem.tag)
    if elem.attrib:
        print(elem.attrib)
    if elem.text:
        print(elem.text.strip())
    for inner_item in elem:
        print('\t=> ', end='')
        if inner_item.tag:
            print('Tags:', end=' ')
            print(inner_item.tag, end='   ')
        if inner_item.attrib:
            print('Attr:', end=' ')
            print(inner_item.attrib, end='   ')
        if inner_item.text:
            print('Text:', end=' ')
            print(inner_item.text, end='   ')
        print()