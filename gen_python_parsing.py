from xml.etree import ElementTree

tree = ElementTree.parse('python.xml')
root = tree.getroot()
print(f'Running code from package: {root.tag}')
code = ''
for elem in root:
    if elem.tag == 'class':
        code += f'class {elem.attrib["name"]}:\n'
        for attr in elem:
            code += ' ' * 4
            if attr.tag == 'attribute':
                code += f'{attr.attrib["name"]} = {attr.text.strip()} \n'
            if attr.tag == 'func':
                func_attr = [attr_name for attr_name in attr.attrib if attr_name != 'name']
                code += f'def {attr.attrib["name"]}({", ".join(func_attr)}):\n'
                for code_line in attr.text.strip().split('\n'):
                    code += ' ' * 8
                    code += code_line.strip()
                    code += '\n'
    if elem.tag == 'code':
        for code_line in elem.text.strip().split('\n'):
            code += code_line.strip()
            code += '\n'
exec(code)
print(result)
